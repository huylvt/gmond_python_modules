#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import time
import urllib2
import traceback

#Metric config
NAME_PREFIX = "lighttpd_"

SERVER_STATUS_URL = ""

Map_Metric = {"Total Processes": NAME_PREFIX + "total_processes",
              "Total Accesses": NAME_PREFIX + "total_accesses",
              "5s Average Accesses": NAME_PREFIX + "5s_avg_accesses",
              "5s Average kBytes": NAME_PREFIX + "5s_avg_kBytes",
              "BusyServers": NAME_PREFIX + "open_connections",
              "Response 200": NAME_PREFIX + "response_200",
              "Response 2xx": NAME_PREFIX + "response_2xx",
              "Response 3xx": NAME_PREFIX + "response_3xx",
              "Response 4xx": NAME_PREFIX + "response_4xx",
              "Response 5xx": NAME_PREFIX + "response_5xx"}

RESPONSE_CODE = [NAME_PREFIX + "response_200",
                 NAME_PREFIX + "response_2xx",
                 NAME_PREFIX + "response_3xx",
                 NAME_PREFIX + "response_4xx",
                 NAME_PREFIX + "response_5xx"]

Process_Metric = {"Request avg (uS)": "_request_avg",
                  "TTFB avg (uS)": "_ttfb_avg",
                  "Response avg (uS)": "_response_avg",
                  "Min Response (uS)": "_min_response",
                  "cache.last-hitrate(%)": "_cache_last_hitrate",
                  "cache.hitrate(%)": "_cache_hitrate",
                  "cache.memory-cached-blocks": "_cache_memory_cached_items",
                  "cache.used-memory-size(MB)": "_cache_used_memory_size"}

descriptors = list()
total_process = 2 
groups = "lighttpd_"


def metric_handler(name):
  
  global SERVER_STATUS_URL, total_process
  metrics = dict()
  SplitInto = lambda L, n: [L[int(i*1.0*len(L)/n):int((i+1)*1.0*len(L)/n)] for i in range(n)]
  try:
    req = urllib2.Request(SERVER_STATUS_URL + "?allauto")
    res = urllib2.urlopen(req, None).read()
    content = res.split("\n\n")
    for info in content[0].split("\n"):
      k, v = info.split(":")
      if k in Map_Metric.keys():
        metrics.update({Map_Metric[k]:int(v.strip())})
    
    #total_process = metrics[NAME_PREFIX + "total_processes"]
    for j in range(1, 3):
      info_process = content[j].split("\n")[1:]
      items_process = SplitInto(info_process, total_process)
      i = 0
      for items in items_process:
        for item in items:
          if ":" in item:
            k, v = item.split(":")
            if k in Process_Metric.keys():
              metrics.update({NAME_PREFIX + "proc_" + str(i) + Process_Metric[k]:int(v.strip())})
        i += 1
    
  except urllib2.URLError:
    traceback.print_exc()
    
  if metrics:
    if name in RESPONSE_CODE:
      value = "%.3f" % ((metrics[name]*100.0)/metrics[NAME_PREFIX + "total_accesses"])
      metrics[name] = float(value)
      
  return metrics[name]


def metric_init(params):
  
  global descriptors, SERVER_STATUS_URL, total_process
   
  if "metric_group" not in params:
    params["metric_group"] = "lighttpd"
     
  if "url" not in params:
    params["url"] = "http://localhost/server-status"
     
  SERVER_STATUS_URL = params["url"]

  descriptors.append({"name": NAME_PREFIX + "5s_avg_accesses",
                      "value_type" : "int",
                      "units"      : "req/sec",
                      "call_back"   : metric_handler,                
                      "format"     : "%d",
                      "description": "5s Average Accesses",
                      "groups":groups + "info"})

  descriptors.append({"name":NAME_PREFIX + "5s_avg_kBytes",
                      "value_type" : "int",
                      "units"      : "kBytes",
                      "call_back"   : metric_handler,                
                      "format"     : "%d",
                      "description": "5s Average kBytes",
                      "groups":groups + "info"})
    
  descriptors.append({"name":NAME_PREFIX + "open_connections",
                      "value_type" : "int",
                      "units"      : "connections",
                      "call_back"   : metric_handler,                
                      "format"     : "%d",
                      "description": "Open Connections",
                      "groups":groups + "info"})

  descriptors.append({"name":NAME_PREFIX + "response_200",
                      "value_type" : "float",
                      "units"      : "percent",
                      "call_back"   : metric_handler,                
                      "format"     : "%.3f",
                      "description": "Response 200",
                      "groups":groups + "info"})
  
  descriptors.append({"name":NAME_PREFIX + "response_2xx",
                      "value_type" : "float",
                      "units"      : "percent",
                      "call_back"   : metric_handler,                
                      "format"     : "%.3f",
                      "description": "Response 2xx",
                      "groups":groups + "info"})

  descriptors.append({"name":NAME_PREFIX + "response_3xx",
                      "value_type" : "float",
                      "units"      : "percent",
                      "call_back"   : metric_handler,                
                      "format"     : "%.3f",
                      "description": "Response 3xx",
                      "groups":groups + "info"})
  
  descriptors.append({"name":NAME_PREFIX + "response_4xx",
                      "value_type" : "float",
                      "units"      : "percent",
                      "call_back"   : metric_handler,                
                      "format"     : "%.3f",
                      "description": "Response 4xx",
                      "groups":groups + "info"})
  
  descriptors.append({"name":NAME_PREFIX + "response_5xx",
                      "value_type" : "float",
                      "units"      : "percent",
                      "call_back"   : metric_handler, 
                      "format"     : "%.3f",
                      "description": "Response 5xx",
                      "groups":groups + "info"})
  for i in range(total_process):

    descriptors.append({"name":NAME_PREFIX + "proc_" + str(i) + "_request_avg",
                        "value_type" : "int",
                        "units"      : "uS",
                        "call_back"   : metric_handler,                
                        "format"     : "%d",
                        "description": "Request avg (uS)",
                        "groups":groups + "process"})
    
    descriptors.append({"name":NAME_PREFIX + "proc_" + str(i) + "_ttfb_avg",
                        "value_type" : "int",
                        "units"      : "uS",
                        "call_back"   : metric_handler,                
                        "format"     : "%d",
                        "description": "TTFB avg (uS)",
                        "groups":groups + "process"})
    
    descriptors.append({"name":NAME_PREFIX + "proc_" + str(i) + "_min_response",
                        "value_type" : "int",
                        "units"      : "uS",
                        "call_back"   : metric_handler,                
                        "format"     : "%d",
                        "description": "Min Response (uS)",
                        "groups":groups + "process"})
    
    descriptors.append({"name":NAME_PREFIX + "proc_" + str(i) + "_response_avg",
                        "value_type" : "int",
                        "units"      : "uS",
                        "call_back"   : metric_handler,                
                        "format"     : "%d",
                        "description": "Response avg (uS)",
                        "groups":groups + "process"})
    
    descriptors.append({"name":NAME_PREFIX + "proc_" + str(i) + "_cache_last_hitrate",
                        "value_type" : "int",
                        "units"      : "percent",
                        "call_back"   : metric_handler,                
                        "format"     : "%d",
                        "description": "cache.last-hitrate(%)",
                        "groups":groups + "cache"})
    
    descriptors.append({"name":NAME_PREFIX + "proc_" + str(i) + "_cache_hitrate",
                        "value_type" : "int",
                        "units"      : "percent",
                        "call_back"   : metric_handler,                
                        "format"     : "%d",
                        "description": "cache.hitrate(%)",
                        "groups":groups + "cache"})
    
    descriptors.append({"name":NAME_PREFIX + "proc_" + str(i) + "_cache_memory_cached_items",
                        "value_type" : "int",
                        "units"      : "MB",
                        "call_back"   : metric_handler,                
                        "format"     : "%d",
                        "description": "cache.memory-cached-items",
                        "groups":groups + "cache"})
    
    descriptors.append({"name":NAME_PREFIX + "proc_" + str(i) + "_cache_used_memory_size",
                        "value_type" : "int",
                        "units"      : "MB",
                        "call_back"   : metric_handler,                
                        "format"     : "%d",
                        "description": "cache.used-memory-size(MB)",
                        "groups":groups + "cache"})
  
  return descriptors


def metric_cleanup():
  '''Clean up the metric module.'''
  pass

if __name__ == '__main__':
  
    try:
        params = {'url': 'http://123.30.239.16/server-status'}
        metric_init(params)
        while True:
            for d in descriptors:
                v = d["call_back"](d["name"])
                print 'value for %s is %s' % (d['name'], v)
        time.sleep(5)
    except KeyboardInterrupt:
        os.exit(1)
