#!/usr/bin/env python
# -*- coding: utf-8 -*-
import copy
import os
import re
import time

LOCATION = "HCM"

if LOCATION == "HN":
    NAME_PREFIX = "net_"
elif LOCATION == "HCM":
    NAME_PREFIX = "net_hcm_"

# data = {"lan":{rx_packets; tx_packets; rx_bytes; tx_bytes},
#         "wan":{rx_packets; tx_packets; rx_bytes; tx_bytes}}
METRICS = {"time": 0,
           "data": {}}

LAST_METRICS = copy.deepcopy(METRICS)
METRICS_CACHE_MAX = 5

socket_metric = ["net_sockets_used",
                 "net_tcp_inuse"]

descriptors = []

net_file = "/proc/net/dev"
sock_file = "/proc/net/sockstat"

groups = "network_monitor"


def get_interfaces():
  
    interfaces = {"lan": [],
                  "wan": [],
                  "wan_vt": [],
                  "wan_vdc": []}
    output = os.popen("ifconfig").read()
    ifaces = output.split("\n\n")
    for iface in ifaces:
        if "inet addr:192.168." in iface:
            lan_iface = iface.split(" ")[0]
            interfaces["lan"].append(lan_iface)
        elif "inet addr:127.0.0.1" in iface:
            continue
        elif "inet addr:210.211.126." in iface:
            wan_iface = iface.split(" ")[0]
            interfaces["wan_vt"].append(wan_iface)
            interfaces["wan"].append(wan_iface)
        elif "inet addr:" in iface and "RX packets:" in iface:
            wan_iface = iface.split(" ")[0]
            interfaces["wan_vdc"].append(wan_iface)
            interfaces["wan"].append(wan_iface)
    return interfaces


def get_metrics():
  """Return all metrics"""
  
  global METRICS, LAST_METRICS, interfaces
  metrics = {NAME_PREFIX + "lan_bytes_in": 0,
             NAME_PREFIX + "lan_packets_in": 0,
             NAME_PREFIX + "lan_bytes_out": 0,
             NAME_PREFIX + "lan_packets_out": 0,
             "net_wan_bytes_in": 0,
             "net_wan_packets_in": 0,
             "net_wan_bytes_out": 0,
             "net_wan_packets_out": 0,
             NAME_PREFIX + "wan_vt_bytes_in": 0,
             NAME_PREFIX + "wan_vt_packets_in": 0,
             NAME_PREFIX + "wan_vt_bytes_out": 0,
             NAME_PREFIX + "wan_vt_packets_out": 0,
             NAME_PREFIX + "wan_vdc_bytes_in": 0,
             NAME_PREFIX + "wan_vdc_packets_in": 0,
             NAME_PREFIX + "wan_vdc_bytes_out": 0,
             NAME_PREFIX + "wan_vdc_packets_out": 0}
  
  if (time.time() - METRICS['time']) > METRICS_CACHE_MAX:
    interfaces = get_interfaces()
    f = open(net_file, "r")
    for line in f:
      if re.search(":", line):
        a = line.split(":")
        dev_name = a[0].lstrip()
        s = re.split("\s+", a[1].lstrip())
        if not re.search("lo", dev_name):
          if dev_name in interfaces["lan"]:
            metrics[NAME_PREFIX + "lan_bytes_in"] = int(s[0])
            metrics[NAME_PREFIX + "lan_packets_in"] = int(s[1])
            metrics[NAME_PREFIX + "lan_bytes_out"] = int(s[8])
            metrics[NAME_PREFIX + "lan_packets_out"] = int(s[9])

          if dev_name in interfaces["wan"]:
            metrics["net_wan_bytes_in"] += int(s[0])
            metrics["net_wan_packets_in"] += int(s[1])
            metrics["net_wan_bytes_out"] += int(s[8])
            metrics["net_wan_packets_out"] += int(s[9])

          if dev_name in interfaces["wan_vt"]:
            metrics[NAME_PREFIX + "wan_vt_bytes_in"] += int(s[0])
            metrics[NAME_PREFIX + "wan_vt_packets_in"] += int(s[1])
            metrics[NAME_PREFIX + "wan_vt_bytes_out"] += int(s[8])
            metrics[NAME_PREFIX + "wan_vt_packets_out"] += int(s[9])

          if dev_name in interfaces["wan_vdc"]:
            metrics[NAME_PREFIX + "wan_vdc_bytes_in"] += int(s[0])
            metrics[NAME_PREFIX + "wan_vdc_packets_in"] += int(s[1])
            metrics[NAME_PREFIX + "wan_vdc_bytes_out"] += int(s[8])
            metrics[NAME_PREFIX + "wan_vdc_packets_out"] += int(s[9])
    
    LAST_METRICS = copy.deepcopy(METRICS)
    METRICS = {"time": time.time(),
               "data": metrics}

  return [METRICS, LAST_METRICS]


def get_sock_metrics():
  
    metrics = {"net_sockets_used": 0,
               "net_tcp_inuse": 0}

    sock_stats = open(sock_file).read().split("\n")
    metrics["net_sockets_used"] = int(sock_stats[0].split(" ")[-1])
    metrics["net_tcp_inuse"] = int(sock_stats[1].split(" ")[2])
    return metrics


def metric_handler(name):
  
  if name in socket_metric:
    metrics = get_sock_metrics()
    delta = metrics[name]
  else:
    [curr_metrics, last_metrics] = get_metrics()
    if last_metrics["data"]:
      delta = (float(curr_metrics['data'][name]) - float(last_metrics['data'][name])) /(curr_metrics['time'] - last_metrics['time'])
    else: 
      delta = 0.0
  return delta

def metric_init(params):
  
  global descriptors

  if "metric_group" not in params:
    params["metric_group"] = "network_monitor"
    
  descriptors.append({"name":NAME_PREFIX + "lan_bytes_in",
                      "value_type" : "float",
                      "units"      : "bytes/sec",
                      "call_back"   : metric_handler,                
                      "format"     : "%.2f",
                      "description": "Lan Bytes In",
                      "groups":groups})
  
  descriptors.append({"name":NAME_PREFIX + "lan_packets_in",
                      "value_type" : "float",
                      "units"      : "packets",
                      "call_back"   : metric_handler,                
                      "format"     : "%.2f",
                      "description": "Lan Packets In",
                      "groups":groups})
  
  descriptors.append({"name":NAME_PREFIX + "lan_bytes_out",
                      "value_type" : "float",
                      "units"      : "bytes/sec",
                      "call_back"   : metric_handler,                
                      "format"     : "%.2f",
                      "description": "Lan Bytes Out",
                      "groups":groups})
  
  descriptors.append({"name":NAME_PREFIX + "lan_packets_out",
                      "value_type" : "float",
                      "units"      : "packets",
                      "call_back"   : metric_handler,                
                      "format"     : "%.2f",
                      "description": "Lan Packets Out",
                      "groups":groups})
  
  descriptors.append({"name": "net_wan_bytes_in",
                      "value_type" : "float",
                      "units"      : "bytes/sec",
                      "call_back"   : metric_handler,                
                      "format"     : "%.2f",
                      "description": "Wan Bytes In",
                      "groups":groups})
  
  descriptors.append({"name": "net_wan_packets_in",
                      "value_type" : "float",
                      "units"      : "packets",
                      "call_back"   : metric_handler,                
                      "format"     : "%.2f",
                      "description": "Wan Packets In",
                      "groups":groups})
  
  descriptors.append({"name": "net_wan_bytes_out",
                      "value_type" : "float",
                      "units"      : "bytes/sec",
                      "call_back"   : metric_handler,                
                      "format"     : "%.2f",
                      "description": "Wan Bytes Out",
                      "groups":groups})
  
  descriptors.append({"name": "net_wan_packets_out",
                      "value_type" : "float",
                      "units"      : "packets",
                      "call_back"   : metric_handler,                
                      "format"     : "%.2f",
                      "description": "Wan Packets Out",
                      "groups":groups})

  descriptors.append({"name":NAME_PREFIX + "wan_vt_bytes_in",
                      "value_type" : "float",
                      "units"      : "bytes/sec",
                      "call_back"   : metric_handler,
                      "format"     : "%.2f",
                      "description": "Viettel Wan Bytes In",
                      "groups":groups})

  descriptors.append({"name":NAME_PREFIX + "wan_vt_packets_in",
                      "value_type" : "float",
                      "units"      : "packets",
                      "call_back"   : metric_handler,
                      "format"     : "%.2f",
                      "description": "Viettel Wan Packets In",
                      "groups":groups})

  descriptors.append({"name":NAME_PREFIX + "wan_vt_bytes_out",
                      "value_type" : "float",
                      "units"      : "bytes/sec",
                      "call_back"   : metric_handler,
                      "format"     : "%.2f",
                      "description": "Viettel Wan Bytes Out",
                      "groups":groups})

  descriptors.append({"name":NAME_PREFIX + "wan_vt_packets_out",
                      "value_type" : "float",
                      "units"      : "packets",
                      "call_back"   : metric_handler,
                      "format"     : "%.2f",
                      "description": "Viettel Wan Packets Out",
                      "groups":groups})

  descriptors.append({"name":NAME_PREFIX + "wan_vdc_bytes_in",
                      "value_type" : "float",
                      "units"      : "bytes/sec",
                      "call_back"   : metric_handler,
                      "format"     : "%.2f",
                      "description": "VDC Wan Bytes In",
                      "groups":groups})

  descriptors.append({"name":NAME_PREFIX + "wan_vdc_packets_in",
                      "value_type" : "float",
                      "units"      : "packets",
                      "call_back"   : metric_handler,
                      "format"     : "%.2f",
                      "description": "VDC Wan Packets In",
                      "groups":groups})

  descriptors.append({"name":NAME_PREFIX + "wan_vdc_bytes_out",
                      "value_type" : "float",
                      "units"      : "bytes/sec",
                      "call_back"   : metric_handler,
                      "format"     : "%.2f",
                      "description": "VDC Wan Bytes Out",
                      "groups":groups})

  descriptors.append({"name":NAME_PREFIX + "wan_vdc_packets_out",
                      "value_type" : "float",
                      "units"      : "packets",
                      "call_back"   : metric_handler,
                      "format"     : "%.2f",
                      "description": "VDC Wan Packets Out",
                      "groups":groups})

  descriptors.append({"name": "net_sockets_used",
                      "value_type" : "int",
                      "units"      : "sockets",
                      "call_back"   : metric_handler,                
                      "format"     : "%d",
                      "description": "Sockets used",
                      "groups":groups})
  
  descriptors.append({"name": "net_tcp_inuse",
                      "value_type" : "int",
                      "units"      : "packets",
                      "call_back"   : metric_handler,                
                      "format"     : "%d",
                      "description": "TCP Inuse",
                      "groups":groups})
  
  return descriptors


def metric_cleanup():
    pass   
  
if __name__ == '__main__':
  
    try:
        params = {}
        metric_init(params)
        while True:
            for d in descriptors:
                v = d["call_back"](d["name"])
                print 'value for %s is %s' % (d['name'], v)
            time.sleep(5)
    except KeyboardInterrupt:
        os.exit(1)
