## Gmond Python Modules
python modules for ganglia 3.1

#### Install các packet cần thiết:

```bash
$ yum install ganglia-gmond-python
```

#### Thêm các modules lighttpd:

```bash
$ cp lighttpd_status.py /usr/lib64/ganglia/python_modules/
$ cp lighttpd_status.pyconf /etc/ganglia/conf.d/
```

#### Thêm modules network_monitor
Theo dõi network theo isp và location

* Set location
```bash
$ vim network_status.py
set biến LOCATION = "HN" hoặc "HCM"
```
* Copy các file cần thiết
```bash
$ cp network_status.py /usr/lib64/ganglia/python_modules/
$ cp network_status.pyconf /etc/ganglia/conf.d/
```

#### Thêm modules hwmon:
theo dõi nhiệt độ CPU/RAM

* Install các gói lm_sensors và kmod-coretemp
 
```bash
$ yum install lm_sensors
$ wget ftp://ftp.pbone.net/mirror/elrepo.org/elrepo/el6/x86_64/RPMS/kmod-coretemp-1.1-10.el6.elrepo.x86_64.rpm
$ rpm -ivh kmod-coretemp-1.1-10.el6.elrepo.x86_64.rpm
$ modprobe coretemp
$ sensors-detect
//Enter để sử dụng các lựa chọn mặc định.  
```

* Copy các file của modules hwmon vào thư mục python_modules và conf.d

```bash
$ cp hwmon.py /usr/lib64/ganglia/python_modules/
$ cp hwmon.pyconf /etc/ganglia/conf.d/
```

#### Restart Gmond

```bash
$ /etc/init.d/gmond restart
```
